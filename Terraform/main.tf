provider "kubernetes" {
    config_path    = "~/.kube/config"
    config_context = "docker-desktop"
}

resource "kubernetes_role" "nodejs_role" {
    metadata {
        name      = "nodejs-role"
        namespace = "nodejs"
}

rule {
    api_groups = ["*"]
    resources  = ["pods"]
    verbs      = ["get", "list", "watch"]
}
}

resource "kubernetes_role_binding" "nodejs_role_binding" {
    metadata {
        name      = "nodejs_role_binding"
        namespace = "nodejs"
}
role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "nodejs-role"
}

subject {
    kind      = "User"
    name      = "john"
    api_group = "rbac.authorization.k8s.io"
}
}