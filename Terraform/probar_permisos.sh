#!/bin/bash
namespace=nodejs
resources="pods deployments"
user=john

echo "=== NAMESPACE: ${namespace} ==="
for verb in create delete get list patch update watch; do
    echo "-- ${verb} --"
    for resource in ${resources}; do
        echo -n "${resource}: "
        kubectl auth can-i ${verb} ${resource} -n ${namespace} --as=${user}
    done
done