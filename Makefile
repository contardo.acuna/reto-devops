.PHONY: help

help:
	@echo "make help                  Desafio 7"
	@echo "make docker                Desafio 1"	
	@echo "make docker-compose        Desafio 2"
	@echo "make install-k8s           Desafio 4"
	@echo "make terraform             Desafio 6"

.PHONY: docker
docker:
	docker build --no-cache -t node app/.
	docker run -d -ti -p 3000:3000 node

.PHONY: docker-compose
docker-compose: 
	docker-compose up -d

.PHONY: install-k8s
install-k8s:
	kubectl apply -f k8s/node-deploy.yaml
	kubectl apply -f k8s/node-service.yaml
	kubectl apply -f k8s/hpa.yaml

.PHONY: terraform
terraform:
	cd Terraform && terraform init
	cd Terraform && terraform plan
	@echo "Applying changes to Infrastracture"
	cd Terraform && terraform apply
	@echo "Clean up after myself"
	$(MAKE) clean
confirm:
	read -r -t 5 -p "Type y to apply, otherwise it will abort (timeout in 5 seconds): " CONTINUE; \
	if [ ! $$CONTINUE == "y" ] || [ -z $$CONTINUE ]; then \
		echo "Abort apply." ; \
		exit 1; \
	fi
